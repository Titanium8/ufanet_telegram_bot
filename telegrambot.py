import requests
import mysql.connector
from mysql.connector import errorcode
import os
from dotenv import load_dotenv
import logging
import time

load_dotenv()

TOKEN = os.getenv('TOKEN')
URL = 'https://api.telegram.org/bot'
host = os.getenv('host')
user = os.getenv('user')
password = os.getenv('password')
db_name = os.getenv('db_name')

logging.basicConfig(filename='bot_messages.log', level=logging.INFO)


def get_database_connection():
    try:
        conn = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=db_name,
        )
        return conn
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Access denied: wrong username or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
        return None


def execute_sql_file(connection, filename):
    with open(filename, 'r') as file:
        sql_commands = file.read().split(';')

        cursor = connection.cursor()

        for command in sql_commands:
            if command.strip():
                try:
                    cursor.execute(command)
                    connection.commit()
                except Exception as e:
                    print(f"Ошибка выполнения SQL-запроса: {e}")

        cursor.close()


def run():
    offset = 0
    retry_delay = 5

    while True:
        conn = None

        while conn is None:
            conn = get_database_connection()

            if conn is None:
                print("Не удалось установить соединение. Повторная попытка через {} секунд.".format(retry_delay))
                time.sleep(retry_delay)

        execute_sql_file(conn, 'database_setup.sql')

        cursor = conn.cursor()

        try:
            result = requests.get(f'{URL}{TOKEN}/getUpdates?offset={offset}').json()
            updates = result.get('result', [])

            for update in updates:
                offset = update['update_id'] + 1
                message = update.get('message', {})
                chat_id = message.get('chat', {}).get('id')
                text = message.get('text', '')

                if text.startswith('/'):
                    command_parts = text.split(' ')
                    command = command_parts[0]
                    command_args = command_parts[1:]
                    process_command(chat_id, command, command_args, conn, cursor)
                else:
                    pass

            if updates:
                last_update_id = updates[-1]['update_id']
                requests.get(f'{URL}{TOKEN}/getUpdates?offset={last_update_id + 1}&timeout=2').json()

        except Exception as e:
            print(f"Произошла ошибка: {e}")
        finally:
            if cursor:
                cursor.close()
            if conn:
                conn.close()


def send_message(chat_id, text):
    requests.get(f'{URL}{TOKEN}/sendMessage?chat_id={chat_id}&text={text}')


def process_command(chat_id, command, command_args, conn, cursor):
    logging.info('Получена команда от пользователя %s: %s', chat_id, command)
    if command == '/start':
        cursor.execute('SELECT * FROM users WHERE telegram_id = %s', (chat_id,))
        existing_user = cursor.fetchone()

        if existing_user:
            user_id = existing_user[0]
            send_message(chat_id, f'Ваш ID - {user_id}')
        else:
            cursor.execute('INSERT INTO users (telegram_id, first_contact_date) VALUES (%s, CURRENT_TIMESTAMP)',
                           (chat_id,))
            conn.commit()
            user_id = cursor.lastrowid
            send_message(chat_id, f'Ваш ID - {user_id}')

    elif command == '/write':
        message_text = ' '.join(command_args).strip()

        if message_text:
            cursor.execute('SELECT id FROM users WHERE telegram_id = %s', (chat_id,))
            user_id_result = cursor.fetchone()

            if user_id_result:
                user_id = user_id_result[0]

                cursor.execute('INSERT INTO messages (user_id, message_text) VALUES (%s, %s)', (user_id, message_text))
                conn.commit()

                send_message(chat_id, f'Заметка {cursor.lastrowid} сохранена')
            else:
                send_message(chat_id, 'Сначала выполните команду /start')
        else:
            send_message(chat_id, 'Введите текст для сохранения в заметке. '
                                  'Пример использования: /write <текст_заметки>')

    elif command == '/read_last':
        cursor.execute(
            'SELECT message_text FROM messages WHERE user_id = (SELECT id FROM users WHERE telegram_id = %s) '
            'ORDER BY timestamp DESC LIMIT 1', (chat_id,))
        result = cursor.fetchone()

        if result:
            last_message = result[0]
            send_message(chat_id, f'Последнее сообщение: {last_message}')
        else:
            send_message(chat_id,
                         'У вас нет сохраненных сообщений или пользователь не найден. Сначала выполните команду /start')

    elif command == '/read':
        if len(command_args) == 1:
            message_id = command_args[0]

            cursor.execute('SELECT user_id FROM messages WHERE id = %s', (message_id,))
            user_id_result = cursor.fetchone()

            if user_id_result:
                message_owner_id = user_id_result[0]

                cursor.execute('SELECT id FROM users WHERE telegram_id = %s', (chat_id,))
                user_id_result = cursor.fetchone()

                if user_id_result:
                    user_id = user_id_result[0]

                    if user_id == message_owner_id:
                        cursor.execute('SELECT message_text FROM messages WHERE id = %s', (message_id,))
                        result = cursor.fetchone()

                        if result:
                            send_message(chat_id, f'Сообщение {message_id}: {result[0]}')
                        else:
                            send_message(chat_id, f'Заметка {message_id} не найдена')
                    else:
                        send_message(chat_id, f'Заметка {message_id} принадлежит другому пользователю')
                else:
                    send_message(chat_id, 'Пользователь не найден. Сначала выполните команду /start')
            else:
                send_message(chat_id, f'Заметка {message_id} не найдена')
        else:
            send_message(chat_id, 'Неверное количество аргументов. '
                                  'Пример использования: /read <ID_заметки>. Например, /read 1')

    elif command == '/read_all':
        cursor.execute(
            'SELECT id, message_text FROM messages WHERE user_id = (SELECT id FROM users WHERE telegram_id = %s) '
            'ORDER BY timestamp', (chat_id,))
        results = cursor.fetchall()

        if results:
            all_messages = "\n".join([f"{message[0]}. {message[1]}" for message in results])
            send_message(chat_id, f'Ваши заметки:\n{all_messages}')
        else:
            send_message(chat_id,
                         'У вас нет сохраненных сообщений или пользователь не найден. Сначала выполните команду /start')

    elif command == '/read_tag':
        if len(command_args) >= 1:
            tag_input = command_args[0].lower()

            if tag_input.startswith('#'):
                tag_to_search = tag_input[1:]
            else:
                tag_to_search = tag_input

            cursor.execute('SELECT COUNT(*) FROM tags WHERE tag = %s', (tag_to_search,))
            tag_count = cursor.fetchone()[0]
            if tag_count > 0:
                cursor.execute(
                    'SELECT message_text FROM messages WHERE user_id = (SELECT id FROM users WHERE telegram_id = %s)'
                    'AND POSITION(%s IN message_text) > 0', (chat_id, tag_input))
                results = cursor.fetchall()

                if results:
                    messages = [result[0] for result in results]
                    message_str = "\n".join(messages)
                    send_message(chat_id, f'Сообщения с тегом {tag_input}:\n{message_str}')
                else:
                    send_message(chat_id, f'Сообщений с тегом {tag_input} не найдено.')
            else:
                send_message(chat_id, f'Тег {tag_input} не найден в базе данных.')
        else:
            send_message(chat_id, 'Неверное использование команды. Пример использования: /read_tag <тег>')

    elif command == '/write_tag':
        if len(command_args) >= 2:
            tag = command_args[0].lower()
            description = ' '.join(command_args[1:]).strip()

            if '#' not in tag and '#' not in description:
                cursor.execute(
                    'INSERT INTO tags (tag, description) VALUES (%s, %s) ON DUPLICATE KEY UPDATE '
                    'description = VALUES(description)', (tag, description))
                conn.commit()

                send_message(chat_id, f'Тег "{tag}" с описанием "{description}" успешно сохранен или обновлен.')
            else:
                send_message(chat_id, 'Теги с решеткой не могут быть сохранены в базе данных.')
        else:
            send_message(chat_id,
                         'Неверное использование команды. Пример использования: /write_tag <тег> <описание_тега>')

    elif command == '/tag':
        if len(command_args) == 1:
            tag_to_search = command_args[0].lower()
            cursor.execute('SELECT description FROM tags WHERE tag = %s', (tag_to_search,))
            tag_description = cursor.fetchone()

            if tag_description:
                send_message(chat_id, f'Описание тега {tag_to_search}: {tag_description[0]}')
            else:
                send_message(chat_id, f'Тег {tag_to_search} не найден.')
        elif len(command_args) > 1:
            tags_to_search = tuple(command_args)
            cursor.execute('SELECT tag, description FROM tags WHERE tag IN ({})'
                           .format(','.join(['%s']*len(tags_to_search))), tags_to_search)

            selected_tags_info = cursor.fetchall()

            if selected_tags_info:
                selected_tags_descriptions = "\n".join([f'{index + 1}. {tag[0]}: {tag[1]}'
                                                        for index, tag in enumerate(selected_tags_info)])
                send_message(chat_id, f'Описания выбранных тегов:\n{selected_tags_descriptions}')
            else:
                send_message(chat_id, 'Нет сохраненных тегов из указанных.')
        else:
            send_message(chat_id, 'Пожалуйста, укажите тег(и) для поиска описаний.')

    elif command == '/tag_all':
        cursor.execute('SELECT description FROM tags')
        tag_info = cursor.fetchall()
        if tag_info:
            tag_descriptions = "\n".join([f'{index + 1}. \\{tag[0]}.{tag[1]}' if len(tag) > 1
                                          else f'{index + 1}. {tag[0]}' for index, tag in enumerate(tag_info)])
            send_message(chat_id, f'Описания всех тегов:\n{tag_descriptions}')
        else:
            send_message(chat_id, 'Нет сохраненных тегов')
    return None


if __name__ == '__main__':
    run()
