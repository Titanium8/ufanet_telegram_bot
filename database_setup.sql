CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    telegram_id INT,
    first_contact_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS messages (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    message_text TEXT,
    timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS tags (
    tag VARCHAR(255) PRIMARY KEY,
    description TEXT
);

INSERT INTO tags (tag, description) VALUES
    ('python', '��������������� ���� ����������������, ��������������� �� ��������� ������������������ ������������ � ���������� ����'),
    ('����������', '��������, ���������, ������, ���������� �� ����� �� �������������')
ON DUPLICATE KEY UPDATE description = VALUES(description);
